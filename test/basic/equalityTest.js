/**
 * Created with IntelliJ IDEA.
 * User: HaoWu
 * Date: 1/24/13
 * Time: 3:44 PM
 */

/**
 The Non-strict Equality Comparison Algorithm

 When performing conversions, the equal and not-equal operators follow these basic rules:

 • If an operand is a Boolean value, convert it into a numeric value before checking for equality. A value of false converts to 0, whereas a value of true converts to 1.
 • If one operand is a string and the other is a number, attempt to convert the string into a number before checking for equality.
 • If one of the operands is an object and the other is not, the valueOf() method is called on the object to retrieve a primitive value to compare according to the previous rules.

 The operators also follow these rules when making comparisons:

 • Values of null and undefined are equal.
 • Values of null and undefined cannot be converted into any other values for equality checking.
 • If either operand is NaN, the equal operator returns false and the not-equal operator returns true. Important note: even if both operands are NaN, the equal operator returns false because, by rule, NaN is not equal to NaN.
 • If both operands are objects, then they are compared to see if they are the same object. If both operands point to the same object, then the equal operator returns true. Otherwise, the two are not equal.
 */

TestCase('Equality Test', {
    'test compare string and numbers' : function() {
        //strict equality
        assertFalse('5' === 5);
        //non-strict equality
        assertTrue('5' == 5);
        assertTrue('+5' == 5);

        // when convert string into a number, Number() constructor is used
        assertFalse('5a' == 5);
    },
    'test compare boolean and number' : function() {
        assertFalse(false == 2);
        assertFalse(true == 2);

        assertTrue(true == 1);
        assertTrue(false == 0);
    },
    'test compare boolean and string' : function() {
        assertFalse(true == 'true');
        assertFalse(false == 'false');

        assertTrue(true == '1');
        assertTrue(false == '0');
    },
    'test compare object and boolean' : function() {
        var trueObj = new Boolean(true);
        assertTrue(true == trueObj);

        var intObj = new Number('1');
        assertTrue(true == intObj);
        // Objects are compared to see if they are the same
        assertFalse(intObj == trueObj);
    },
    'test compare object': function() {
        var intObj1 = new Number('1');
        var intObj2 = new Number('1');

        assertFalse(intObj1 == intObj2);
    }
});